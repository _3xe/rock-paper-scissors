// First own project in JS

(function rockPaperScissors() {
  const elements = document.querySelectorAll('.choose');

  const title = document.querySelector('h1');

  const chooseSection = document.querySelector('.options');
  const warSection = document.querySelector('.war');

  const user = document.querySelector('.chosen');
  const computer = document.querySelector('.computer');

  const userImg = user.getElementsByClassName('img')[0];
  const computerImg = computer.getElementsByClassName('img')[0];

  let userWins = document.querySelector('.userWins');
  let computerWins = document.querySelector('.computerWins');

  const win = document.querySelector('.win');
  const loss = document.querySelector('.loss');
  const tied = document.querySelector('.tied');

  let round = document.querySelector('#countRound');
  let roundCount = 1;
  let userCount = 0;
  let computerCount = 0;

  let array = new Array('rock.png', 'paper.png', 'scissors.png');

  elements.forEach(element => element.addEventListener('click', () => {

    let index = element.src.lastIndexOf('/');
    let src = element.src.slice(index + 1);

    let random = Math.floor(Math.random() * (2 - 0 + 1) + 0);

    function showWar () {
      return new Promise(res => {
        setTimeout(() => {
          title.innerHTML = "Time for WAR!";
          chooseSection.style.display="none";
          warSection.style.display="flex";
          userImg.src=src;
          computerImg.src="question.png";
          res();
        },500)
      }) 
    }

    function slide () {
      return new Promise(res => {
        setTimeout(() => {
          user.classList.add('active');
          computer.classList.add('active');
          computerImg.src = array[random];
          res()
        },500)
      })

    }

    function checkWho () {
      return new Promise(res => {
        setTimeout( () => {
          let userSrc = src;
          let computerSrc = array[random];

          if (

            (userSrc === "paper.png" && computerSrc === "rock.png") ||
            (userSrc === "rock.png"  && computerSrc === "scissors.png") ||
            (userSrc === "scissors.png"  && computerSrc === "paper.png")

            ) {

            user.classList.add('checkRound');
            computer.classList.add('checkRound');
            win.style.display="block";
            userCount += 1;
            userWins.innerHTML = userCount;
            
            } else if (computerSrc === userSrc) {
            
            computer.classList.add('checkRound');
            user.classList.add('checkRound');
            tied.style.display="block";

            } else {

            user.classList.add('checkRound');
            computer.classList.add('checkRound');
            loss.style.display="block";
            computerCount += 1;
            computerWins.innerHTML = computerCount;
            
            }

            res();
            
        },500)
      })
    }

    function nextGame () {
      return new Promise(res => {
        setTimeout( () => {
          roundCount += 1;
          round.innerHTML = roundCount;
          computer.classList.remove('active');
          computer.classList.remove('checkRound');
          computer.classList.remove('loser');
          user.classList.remove('active');
          user.classList.remove('checkRound');
          user.classList.remove('loser');
          win.style.display="none";
          loss.style.display="none";
          tied.style.display="none";
          title.innerHTML = "Choose your option...";

          warSection.style.display="none";
          chooseSection.style.display="flex";
          res();
        },1500)
      })
    }

    (async function() {
      await showWar();
      await slide();
      await checkWho();
      await nextGame();
    })()

  }));

})();

